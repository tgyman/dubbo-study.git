package com.tgy.service;
//一定要导入的是dubbo中的service
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;
//import org.springframework.stereotype.Service;

@Service
@Component
public class TicketServiceImpl implements TicketService {
    @Override
    public String getTicket() {
        return "在12306网站购买了10张票";
    }
}
