package com.tgy.service;

/**
 * RPC接口
 * @author tgyman
 */
public interface TicketService {
    String getTicket();
}
