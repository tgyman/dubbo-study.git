package com.tgy.controller;

import com.tgy.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 购票服务
 */
@Controller
@RequestMapping("ticket")
public class TicketController {

    @Autowired
    UserService userService;


    @RequestMapping("buy")
    @ResponseBody
    public String buy(){
        String ticket = userService.buyTicket();
        return ticket;
    }
}
