package com.tgy.service;

import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.stereotype.Service;

/**
 * 消费者的服务类
 */
@Service
public class UserService {

    /**
     * 远程引用指定的服务
     */
   @DubboReference
    TicketService ticketService;

    public String buyTicket(){
       return ticketService.getTicket();
    }
}
